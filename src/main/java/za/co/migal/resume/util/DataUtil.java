package za.co.migal.resume.util;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import za.co.migal.resume.models.*;
import za.co.migal.resume.services.AboutMeService;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.*;
import java.net.URL;
import java.net.URLConnection;
import java.nio.file.Files;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Log4j2
public class DataUtil {
    private AboutMeService aboutMeService;
    @Value("${setup.initial.file}")
    String setupFile;

    private ObjectMapper objectMapper;

    public  void initializeExtendedProfile() throws IOException {
        ExtendedProfile extendedProfile =  objectMapper.readValue(getFileFromUrl(setupFile), ExtendedProfile.class);
        aboutMeService.addExtendedProfile(extendedProfile);

        List<Education> educationList = getEducation();
        educationList.stream().forEach(education -> aboutMeService.addEducation(education));

    }

    public void clear() {
        aboutMeService.clear();
    }

    public ExtendedProfile getMigalsExtendedProfile() {
        ExtendedProfile profile = new ExtendedProfile();
        profile.setAboutMe("this is about me");
        profile.setAddress(new Address());
        profile.getAddress().setStreetName("6 wavely street");
        profile.getAddress().setCity("Palet Town");
        profile.setDateOfBirth(LocalDate.of(1990,7,5));
        profile.setEmailAddress("migal@gmail.com");
        profile.setFirstName("Migal");
        profile.setLastName("Lewis");
        profile.setImage("");
        profile.setOccupation("Software Developer/ Tech Lead");
        profile.setPhoneNumber("074 863 9773");
        profile.setId("1");

        setGoogleItems(profile);
        setFavouritePokemon(profile);
        setSkills(profile);
        setExperience(profile);

        profile.setMessage("My definition of a successful person is a person " +
                "who has changed the world in someway");
        return  profile;
    }

    private void setFavouritePokemon(ExtendedProfile profile) {
        profile.setFavoritePokemonNo(Arrays.asList(4,197,330,94,448,778));
    }


    private void setGoogleItems(ExtendedProfile profile) {
        profile.setGoogleItems(new ArrayList<>());
        profile.getGoogleItems().add(new ExtendedProfile.GoogleItem());
        profile.getGoogleItems().get(0).setId(1);
        profile.getGoogleItems().get(0).setName("Technology");
        profile.getGoogleItems().get(0).setExtraInfo("always interested in the latest tech");
        profile.getGoogleItems().get(0)
                .setImage("https://images.pexels.com/photos/834949/pexels-photo-834949.jpeg?" +
                        "auto=compress&cs=tinysrgb&dpr=2&h=50&w=50");
        profile.getGoogleItems().get(0).setGoogleExamples(new ArrayList<>());
        profile.getGoogleItems().get(0).getGoogleExamples().add(new ExtendedProfile.GoogleExample());
        profile.getGoogleItems().get(0).getGoogleExamples().get(0).setName("Virtual Reality");
        profile.getGoogleItems().get(0).getGoogleExamples().get(0).
                setImage("https://images.pexels.com/photos/834949/pexels-photo-834949.jpeg?auto=compress&cs=tinysrgb&dpr=3&h=50&w=70");
        profile.getGoogleItems().get(0).getGoogleExamples().add(new ExtendedProfile.GoogleExample());
        profile.getGoogleItems().get(0).getGoogleExamples().get(1).setName("Argumented Reality");
        profile.getGoogleItems().get(0).getGoogleExamples().get(1).
                setImage("https://images.pexels.com/photos/163042/pokemon-pokemon-go-mobile-trends-smartphone-163042.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=50&w=70");
        profile.getGoogleItems().get(0).getGoogleExamples().add(new ExtendedProfile.GoogleExample());
        profile.getGoogleItems().get(0).getGoogleExamples().get(2).setName("solid state battery");
        profile.getGoogleItems().get(0).getGoogleExamples().get(2).
                setImage("https://images.pexels.com/photos/258083/pexels-photo-258083.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=50&w=70");
        profile.getGoogleItems().get(0).getGoogleExamples().add(new ExtendedProfile.GoogleExample());
        profile.getGoogleItems().get(0).getGoogleExamples().get(3).setName("Graphene");
        profile.getGoogleItems().get(0).getGoogleExamples().get(3).
                setImage("https://analyticalscience.wiley.com/do/10.1002/gitlab" +
                        ".15487/full/ib09424569f266f6d1c1af4a8473bc5c0.jpg");
        profile.getGoogleItems().get(0).getGoogleExamples().get(3).setExtraInfo("https://analyticalscience.wiley" +
                ".com/do/10.1002/gitlab.15487/full/");
        profile.getGoogleItems().get(0).getGoogleExamples().add(new ExtendedProfile.GoogleExample());
        profile.getGoogleItems().get(0).getGoogleExamples().get(4).setName("Sky Scraper Concept");
        profile.getGoogleItems().get(0).getGoogleExamples().get(4).
                setImage("https://assets.newatlas.com/dims4/default/6cc7d10/2147483647/strip/true/crop/2400x1600+0+85/resize/2400x1600!/format/webp/quality/90/?url=http%3A%2F%2Fnewatlas-brightspot.s3.amazonaws.com%2Fdc%2F0f%2Ff1766fa84faea466283a5ceb8a3e%2Fvertical-emergency-skyscraper-0.jpg");
        profile.getGoogleItems().get(0).getGoogleExamples().get(4).setExtraInfo("https://newatlas.com/architecture/high-rise-hospital-concept-evolo-2020-skyscraper/");
        profile.getGoogleItems().get(0).getGoogleExamples().add(new ExtendedProfile.GoogleExample());
        profile.getGoogleItems().get(0).getGoogleExamples().get(5).setName("IOT");
        profile.getGoogleItems().get(0).getGoogleExamples().get(5).
                setImage("https://images.pexels.com/photos/1476321/pexels-photo-1476321.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=50&w=70");


        profile.getGoogleItems().add(new ExtendedProfile.GoogleItem());
        profile.getGoogleItems().get(1).setRank(2);
        profile.getGoogleItems().get(1).setId(2);
        profile.getGoogleItems().get(1).setName("Software");
        profile.getGoogleItems().get(1).setExtraInfo("stackoverflow, codepen , " +
                "medium thank you for the help you have given me through my career");
        profile.getGoogleItems().get(1)
                .setImage("https://images.pexels.com/photos/546819/pexels-photo-546819.jpeg?" +
                        "auto=compress&cs=tinysrgb&dpr=2&h=50&w=50");


        profile.getGoogleItems().add(new ExtendedProfile.GoogleItem());
        profile.getGoogleItems().get(2).setId(3);
        profile.getGoogleItems().get(2).setRank(3);
        profile.getGoogleItems().get(2).setName("Icons");
        profile.getGoogleItems().get(2).setExtraInfo("every now and then it is nice to just read on some" +
                " inspirational things");
        profile.getGoogleItems().get(2)
                .setImage("https://images.pexels.com/photos/6633/car-superhero-symbol-batman.jpg?" +
                        "auto=compress&cs=tinysrgb&dpr=2&h=50&w=50");
        profile.getGoogleItems().add(new ExtendedProfile.GoogleItem());
        profile.getGoogleItems().get(3).setId(4);
        profile.getGoogleItems().get(3).setRank(4);
        profile.getGoogleItems().get(3).setName("Youtube");
        profile.getGoogleItems().get(3).setExtraInfo("really great place for learning, and some entertainment");
        profile.getGoogleItems().get(3)
                .setImage("https://i.pinimg.com/originals/2d/2b/e2/2d2be2421911037d80f9921dc29d54c2.jpg");
        profile.getGoogleItems().add(new ExtendedProfile.GoogleItem());
        profile.getGoogleItems().get(4).setId(5);
        profile.getGoogleItems().get(4).setRank(5);
        profile.getGoogleItems().get(4).setName("DIY");
        profile.getGoogleItems().get(4).setExtraInfo("love building and trying ouy new things");
        profile.getGoogleItems().get(4)
                .setImage("https://images.pexels.com/photos/301703/pexels-photo-301703.jpeg?" +
                        "auto=compress&cs=tinysrgb&dpr=2&h=50&w=50");
        profile.getGoogleItems().add(new ExtendedProfile.GoogleItem());
        profile.getGoogleItems().get(5).setId(6);
        profile.getGoogleItems().get(5).setRank(6);
        profile.getGoogleItems().get(5).setName("Netflix");
        profile.getGoogleItems().get(5).setExtraInfo("I watch atleast one series a day," +
                " Stranger things has to be my favourite from netflix");
        profile.getGoogleItems().get(5)
                .setImage("https://lh3.googleusercontent.com/" +
                        "TBRwjS_qfJCSj1m7zZB93FnpJM5fSpMA_wUlFDLxWAb45T9RmwBvQd5cWR5viJJOhkI");
        profile.getGoogleItems().add(new ExtendedProfile.GoogleItem());
        profile.getGoogleItems().get(6).setId(8);
        profile.getGoogleItems().get(6).setRank(8);
        profile.getGoogleItems().get(6).setName("Soccer/Football");
        profile.getGoogleItems().get(6).setExtraInfo("Latest transfer gossip, Man united , premier league and uefa");
        profile.getGoogleItems().get(6)
                .setImage("https://images.pexels.com/photos/47730/the-ball-stadion-football-the-pitch-47730.jpeg?" +
                        "auto=compress&cs=tinysrgb&dpr=2&h=50&w=50");
        profile.getGoogleItems().add(new ExtendedProfile.GoogleItem());
        profile.getGoogleItems().get(7).setId(7);
        profile.getGoogleItems().get(7).setRank(7);
        profile.getGoogleItems().get(7).setName("Animals");
        profile.getGoogleItems().get(7).setExtraInfo("Whales, Leopard, Octopus, Dogs and Wolves are some of my favourite");
        profile.getGoogleItems().get(7)
                .setImage("https://images.pexels.com/photos/46254/leopard-wildcat-big-cat-botswana-46254.jpeg?" +
                        "auto=compress&cs=tinysrgb&dpr=2&h=50&w=50");
    }


    public void setExperience(ExtendedProfile profile) {
        profile.setExperienceList( new ArrayList<>());
        profile.getExperienceList().add(new Experience());
        profile.getExperienceList().get(0).setCompany("FNB Life");
        profile.getExperienceList().get(0).setImage("../../assets/images/FNB-Bank.png");
        profile.getExperienceList().get(0).setDescription("FNB Life");
        profile.getExperienceList().get(0).setPosition("Intern and then Junior Developer");
        profile.getExperienceList().get(0).setDateFrom(LocalDate.of(2013,2,1));
        profile.getExperienceList().get(0).setDateTo(LocalDate.of(2015,10,31));
        profile.getExperienceList().get(0).setProjects(new ArrayList<>());
        profile.getExperienceList().get(0).getProjects().add(new Experience.Project());
        profile.getExperienceList().get(0).getProjects().get(0).setName("Application Monitor");
        profile.getExperienceList().get(0).getProjects().get(0).setDetails("I worked on and created the Full frontend of this app as well some backend." +
                "I also handled the deployments to the different environments" +
                "The project went live successfully. I now solely maintained and added new features to this application");
        profile.getExperienceList().get(0).getProjects().get(0).setShortDescription("A Dashboard application that " +
                "monitors our other applications");
        profile.getExperienceList().get(0).getProjects().get(0).setTechnologies(Arrays.asList(
                "Jsf-Primefaces ","Javascript","Java 7","JEE 7 ", "EJB 3", "Jboss As 7" ,"Restful webservices",
                "Soap Webservices", "Db2"));
        profile.getExperienceList().get(0).getProjects().get(0).setRole("1 of 3 Developers,I worked on Backend and " +
                "Frontend");
        profile.getExperienceList().get(0).getProjects().add(new Experience.Project());
        profile.getExperienceList().get(0).getProjects().get(1).setName("Icam Daily reporting tool");
        profile.getExperienceList().get(0).getProjects().get(1).setDetails("This is a program that was created to fix an audit exception ,it  connects to different databases and  creates reports on all users using the different systems in Fnb Life and stores it on a server  where the auditing company can pick it daily using their program . It connects to Microsoft sql server, several Db2 schemas, Mysql and Ldap");
        profile.getExperienceList().get(0).getProjects().get(1).setShortDescription("A program that collects user " +
                "information and creates a report");
        profile.getExperienceList().get(0).getProjects().get(1).setTechnologies(Arrays.asList(
                "Wildfly","Hibernate","Java 8","JEE 7 ", "EJB 3", "Jboss As 7" ,"Restful webservices",
                "Jsf-Primefaces"));
        profile.getExperienceList().get(0).getProjects().get(1).setRole("I solely created this small project from " +
                "front to back");
        profile.getExperienceList().get(0).getProjects().add(new Experience.Project());
        profile.getExperienceList().get(0).getProjects().get(2).setName("Voice Recording Migration");
        profile.getExperienceList().get(0).getProjects().get(2).setDetails("When FNB life moved from being " +
                "underwritten by momentum to having own Insurance licence.We had to migrate all customer voice " +
                "recordings from a custom momentum Vox type to mp3 and then link to customer on fnb database.over a " +
                "million voice recordings needed to be migrated  from momentum servers to Fnb servers");
        profile.getExperienceList().get(0).getProjects().get(2).setShortDescription("A script that converted voice " +
                "recordings, migrated to a new server and linked new location to database");
        profile.getExperienceList().get(0).getProjects().get(2).setTechnologies(Arrays.asList(
                " Bash Script","Db2"));
        profile.getExperienceList().get(0).getProjects().get(2).setRole("I solely migrated over a million voice" +
                " recordings from momentum servers to Fnb servers");

        profile.getExperienceList().add(new Experience());
        profile.getExperienceList().get(1).setCompany("Barclays Africa");
        profile.getExperienceList().get(1).setImage("../../assets/images/Barclays-Logo.png");
        profile.getExperienceList().get(1).setDescription("Most probably the worst place I work at");
        profile.getExperienceList().get(1).setPosition("Intermediate Developer");
        profile.getExperienceList().get(1).setDateFrom(LocalDate.of(2013,2,1));
        profile.getExperienceList().get(1).setDateTo(LocalDate.of(2015,10,31));


        profile.getExperienceList().add(new Experience());
        profile.getExperienceList().get(2).setCompany("BBD");
        profile.getExperienceList().get(2).setImage("../../assets/images/BBD-logo.png");
        profile.getExperienceList().get(2).setDescription("Most probably the best place I work at");
        profile.getExperienceList().get(2).setPosition("Intermediate Developer");
        profile.getExperienceList().get(2).setDateFrom(LocalDate.of(2013,2,1));
        profile.getExperienceList().get(2).setDateTo(LocalDate.of(2015,10,31));
        profile.getExperienceList().add(new Experience());
        profile.getExperienceList().get(3).setCompany("'Omnipresent'");
        profile.getExperienceList().get(3).setImage("../../assets/images/omnipresent.jpg");
        profile.getExperienceList().get(3).setDescription("Most probably the worst place I work at");
        profile.getExperienceList().get(3).setPosition("Senior Developer, Tech Lead");
        profile.getExperienceList().get(3).setDateFrom(LocalDate.of(2019,2,1));
        profile.getExperienceList().get(3).setDateTo(LocalDate.of(2020,5,1));
    }

    List<Education> getEducation() {
        List<Education> educationList = new ArrayList<>();
        educationList.add(new Education());
        educationList.get(0).setImage("../../assets/images/school.png");
        educationList.get(0).setInstitute("Primrose High School");
        educationList.get(0).setCertificationName("Matric'");
        educationList.add(new Education());
        educationList.get(1).setImage("../../assets/images/university.png");
        educationList.get(1).setInstitute("Nelson Mandela University");
        educationList.get(1).setCertificationName("None");
        educationList.add(new Education());
        educationList.get(2).setImage("../../assets/images/Learnership.jpg");
        educationList.get(2).setInstitute("FNB Learnership");
        educationList.get(2).setCertificationName("National Certificate:Information Technologies");
        return educationList;
    }

    private void setSkills(ExtendedProfile profile) {
        profile.setSkills(new ArrayList<>());
        profile.getSkills().add(new Skill());
        profile.getSkills().get(0).setId(1);
        profile.getSkills().get(0).setName("Java");
        profile.getSkills().get(0).setScore(8);
        profile.getSkills().get(0).setYearsExperience(7);
        profile.getSkills().get(0).setImage("../../assets/images/skills/java.png");
        profile.getSkills().add(new Skill());
        profile.getSkills().get(1).setId(2);
        profile.getSkills().get(1).setName("Springboot");
        profile.getSkills().get(1).setScore(8);
        profile.getSkills().get(1).setYearsExperience(3);
        profile.getSkills().get(1).setImage("../../assets/images/skills/springboot.png");
        profile.getSkills().add(new Skill());
        profile.getSkills().get(2).setId(3);
        profile.getSkills().get(2).setName("Angular");
        profile.getSkills().get(2).setScore(8);
        profile.getSkills().get(2).setYearsExperience(3);
        profile.getSkills().get(2).setImage("../../assets/images/skills/angular.png");
        profile.getSkills().add(new Skill());
        profile.getSkills().get(3).setId(4);
        profile.getSkills().get(3).setName("Html");
        profile.getSkills().get(3).setScore(9);
        profile.getSkills().get(3).setYearsExperience(6);
        profile.getSkills().get(3).setImage("../../assets/images/skills/html.png");
        profile.getSkills().add(new Skill());
        profile.getSkills().get(4).setId(5);
        profile.getSkills().get(4).setName("Css");
        profile.getSkills().get(4).setScore(7);
        profile.getSkills().get(4).setYearsExperience(6);
        profile.getSkills().get(4).setImage("../../assets/images/skills/css.png");
        profile.getSkills().add(new Skill());
        profile.getSkills().get(5).setId(6);
        profile.getSkills().get(5).setName("Javascript");
        profile.getSkills().get(5).setScore(6);
        profile.getSkills().get(5).setYearsExperience(3);
        profile.getSkills().get(5).setImage("../../assets/images/skills/javascript.png");
        profile.getSkills().add(new Skill());
        profile.getSkills().get(6).setId(7);
        profile.getSkills().get(6).setName("Bash");
        profile.getSkills().get(6).setScore(7);
        profile.getSkills().get(6).setImage("../../assets/images/skills/bash.png");
        profile.getSkills().get(6).setYearsExperience(6);
        profile.getSkills().add(new Skill());
        profile.getSkills().get(7).setId(8);
        profile.getSkills().get(7).setName("Kuberenetes");
        profile.getSkills().get(7).setScore(7);
        profile.getSkills().get(7).setYearsExperience(3);
        profile.getSkills().get(7).setImage("../../assets/images/skills/kubernetes.png");
        profile.getSkills().add(new Skill());
        profile.getSkills().get(8).setId(9);
        profile.getSkills().get(8).setName("Openshift");
        profile.getSkills().get(8).setScore(7);
        profile.getSkills().get(8).setYearsExperience(2);
        profile.getSkills().get(8).setImage("../../assets/images/skills/openshift.png");

    }


    public JsonNode getJsonNode(String fileName) throws IOException {
        ObjectMapper objectMapper =new ObjectMapper();
        return objectMapper.readTree(getFileFromUrl(fileName));
    }

    public static String getFileAsString(String fileName) throws IOException {
        ClassLoader classLoader = DataUtil.class.getClassLoader();
        File file = new File(classLoader.getResource("data/"+fileName).getFile());
        return "file://" + file.toURI().toURL().getFile();
    }

    public void setSetupFile(String setupFile) {
        this.setupFile = setupFile;
    }


    String getFileFromUrl(String urlString) throws IOException {
        if(!urlString.startsWith("http") && !urlString.startsWith("file")){
            urlString = getFileAsString(urlString);
        }
        URL url = new URL(urlString);
        URLConnection connection = url.openConnection();
        try(BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(connection.getInputStream()))) {
            String line;
            StringBuilder response = new StringBuilder();
            while ((line = bufferedReader.readLine()) != null) {
                response.append(line);
            }
            return response.toString();
        }
    }

    public String getSetupFile() {
        return setupFile;
    }

    @Autowired
    public void setAboutMeService(AboutMeService aboutMeService) {
        this.aboutMeService = aboutMeService;
    }

    @Autowired
    public void setObjectMapper(ObjectMapper objectMapper) {
        this.objectMapper = objectMapper;
    }
}
