package za.co.migal.resume.services;

import lombok.extern.log4j.Log4j2;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import za.co.migal.resume.models.*;
import za.co.migal.resume.repository.EducationRepository;
import za.co.migal.resume.repository.ExtendedProfileRepository;

import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;

@Log4j2
@Service("aboutMeService")
public class AboutMeServiceImpl implements AboutMeService {
    private ExtendedProfileRepository extendedProfileRepository;
    private EducationRepository educationRepository;
    private ModelMapper modelMapper;
    private PokemonService pokemonService;

    @Override
    public ProfileResponse getSummary(String name) {
        ExtendedProfile extendedProfile = extendedProfileRepository.findByFirstName(name);
        return modelMapper.map(extendedProfile, ProfileResponse.class);
    }


    @Override
    public String addExtendedProfile(ExtendedProfile extendedProfile) {
        return extendedProfileRepository.save(extendedProfile).getId();
    }


    @Override
    public Education addEducation(Education education) {
        return educationRepository.save(education);
    }

    @Override
    public List<Education> getEducation() {
        return educationRepository.findAll();
    }

    @Override
    public void clear() {
        extendedProfileRepository.deleteAll();
        educationRepository.deleteAll();
    }

    @Override
    public ExtendedProfileResponse getAllDetails(String name) {
        ExtendedProfile profile = extendedProfileRepository.findByFirstName(name);

        List<Pokemon> pokemonList = new ArrayList<>();
        profile.getFavoritePokemonNo().stream().forEach(no -> {
            try {
                pokemonList.add(pokemonService.getPokemonData(no));
            } catch (URISyntaxException e) {
                log.error(e);
            }
        });

        ExtendedProfileResponse response = modelMapper.map(profile, ExtendedProfileResponse.class);
        response.setFavouritePokemon(pokemonList);
        return response;
    }


    @Autowired
    public void setExtendedProfileRepository(ExtendedProfileRepository extendedProfileRepository) {
        this.extendedProfileRepository = extendedProfileRepository;
    }

    @Autowired
    public void setModelMapper(ModelMapper modelMapper) {
        this.modelMapper = modelMapper;
    }


    @Autowired
    public void setEducationRepository(EducationRepository educationRepository) {
        this.educationRepository = educationRepository;
    }

    @Autowired
    public void setPokemonService(PokemonService pokemonService) {
        this.pokemonService = pokemonService;
    }
}
