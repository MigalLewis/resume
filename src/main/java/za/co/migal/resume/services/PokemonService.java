package za.co.migal.resume.services;

import za.co.migal.resume.models.Pokemon;

import java.net.URISyntaxException;

public interface PokemonService {
    Pokemon getPokemonData(int number) throws URISyntaxException;
}
