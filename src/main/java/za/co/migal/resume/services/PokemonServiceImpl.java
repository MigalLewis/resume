package za.co.migal.resume.services;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ArrayNode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import za.co.migal.resume.models.Pokemon;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Service("pokemonService")
public class PokemonServiceImpl implements PokemonService {
    private RestTemplate restTemplate;
    @Value("${api.pokemon.url}")
    private String baseUrl;
    private static final String POKEMON_URL  = "/pokemon/";
    private static final String POKEMON_SPECIES_URL  = "/pokemon-species/";

    @Override
    public Pokemon getPokemonData(int number) throws URISyntaxException {
        URI uri = new URI(baseUrl+POKEMON_URL+number);
        URI spiciesUri = new URI(baseUrl+POKEMON_SPECIES_URL+number);

        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        headers.add("user-agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.99 Safari/537.36");


        HttpEntity<String> entity = new HttpEntity<String>("parameters", headers);

        ResponseEntity<JsonNode>  response = restTemplate.exchange(uri,
                HttpMethod.GET,entity,JsonNode.class);

        ResponseEntity<JsonNode>  speciesResponse = restTemplate.exchange(spiciesUri,
                HttpMethod.GET,entity,JsonNode.class);

        Pokemon pokemon = new Pokemon();
        mapBasicPokemonDetails(pokemon, response.getBody());
        mapPokemonSpeciesDetails(pokemon, speciesResponse.getBody());

        return pokemon;
    }

    public void mapBasicPokemonDetails(Pokemon pokemon, JsonNode jsonNode) {
        pokemon.setName(jsonNode.get("name").asText());
        pokemon.setHeight(jsonNode.get("height").asText());
        pokemon.setWeight(jsonNode.get("weight").asText());
        pokemon.setId(jsonNode.get("id").asInt());
        pokemon.setImage(jsonNode.at("/sprites/front_default").asText());
        pokemon.setTypes( new ArrayList<>());
        ArrayNode typesArrayNode = (ArrayNode) jsonNode.findValues("types", new ArrayList<>()).get(0);
        typesArrayNode.forEach(t -> pokemon.getTypes().add(t.at("/type/name").asText()));

    }
    public void mapPokemonSpeciesDetails(Pokemon pokemon, JsonNode jsonNode) {
        List<JsonNode> genera = jsonNode.findValues("genera");
        if(genera.get(0).isArray()) {
            ArrayNode arrayNode = (ArrayNode) genera.get(0);
            List<JsonNode> generas = new ArrayList<>();
            arrayNode.forEach(g -> generas.add(g));
            JsonNode english = generas.stream().filter(
                    g -> g.at("/language/name").asText().equals("en")).findFirst().get();
            pokemon.setSpecies(english.get("genus").asText());
        }

        List<JsonNode> flavorTextEntries = jsonNode.findValues("flavor_text_entries");
        if(flavorTextEntries.get(0).isArray()) {
            ArrayNode arrayNode = (ArrayNode) flavorTextEntries.get(0);
            List<JsonNode> textArray = new ArrayList<>();
            arrayNode.forEach(g -> textArray.add(g));
            JsonNode english = textArray.stream().filter(
                    g -> g.at("/language/name").asText().equals("en")).findFirst().get();
            pokemon.setDescription(english.get("flavor_text").asText());
        }
    }

    @Autowired
    public void setRestTemplate(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }
}
