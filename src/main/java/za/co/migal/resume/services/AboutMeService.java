package za.co.migal.resume.services;

import za.co.migal.resume.models.Education;
import za.co.migal.resume.models.ExtendedProfile;
import za.co.migal.resume.models.ExtendedProfileResponse;
import za.co.migal.resume.models.ProfileResponse;

import java.util.List;

public interface AboutMeService {
    ProfileResponse getSummary(String name);
    ExtendedProfileResponse getAllDetails(String name);
    String addExtendedProfile(ExtendedProfile extendedProfile);

    Education addEducation(Education education);
    List<Education> getEducation();

    void clear();
}
