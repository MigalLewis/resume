package za.co.migal.resume.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import za.co.migal.resume.models.ProfileResponse;

public interface ProfileRepository extends MongoRepository<ProfileResponse, String> {
    ProfileResponse findByFirstName(String firstName);

}
