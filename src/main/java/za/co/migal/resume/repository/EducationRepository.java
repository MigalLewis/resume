package za.co.migal.resume.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import za.co.migal.resume.models.Education;

public interface EducationRepository extends MongoRepository<Education, String> {

}
