package za.co.migal.resume.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import za.co.migal.resume.models.ExtendedProfile;

public interface ExtendedProfileRepository extends MongoRepository<ExtendedProfile, String> {
    ExtendedProfile findByFirstName(String firstName);
}
