package za.co.migal.resume.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import za.co.migal.resume.models.*;
import za.co.migal.resume.services.AboutMeService;

import java.util.List;

@RestController
@RequestMapping(value = "/about/me")
public class AboutMeController {
    @Autowired
    private AboutMeService aboutMeService;

    @GetMapping("/profile/basic/{name}")
    public @ResponseBody
    ProfileResponse getProfile(@PathVariable String name) {
        return aboutMeService.getSummary(name);
    }

    @GetMapping("/profile/extended/{name}")
    public @ResponseBody ExtendedProfileResponse getProfileExtended(@PathVariable String name) {
        return aboutMeService.getAllDetails(name);
    }

    @PostMapping("/profile/extended")
    public String saveProfileExtended(@RequestBody ExtendedProfile extendedProfile) {
        return aboutMeService.addExtendedProfile(extendedProfile);
    }
    @GetMapping("/skills")
    public @ResponseBody List<Skill> getSkills() {
        return aboutMeService.getAllDetails("Migal").getSkills();
    }

    @GetMapping("/experience")
    public @ResponseBody List<Experience> getExperience() {
        return aboutMeService.getAllDetails("Migal").getExperienceList();
    }

    @GetMapping("/education")
    public @ResponseBody List<Education> getEducation() {
        return aboutMeService.getEducation();
    }
}
