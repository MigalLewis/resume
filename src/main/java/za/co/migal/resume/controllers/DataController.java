package za.co.migal.resume.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import za.co.migal.resume.util.DataUtil;

import java.io.IOException;

@RestController
@RequestMapping(value = "/data")
public class DataController {
    @Autowired
    DataUtil dataUtil;

    @PostMapping("/initialize")
    public void addData() throws IOException {
        dataUtil.initializeExtendedProfile();
    }

    @DeleteMapping("/clear")
    public void deleteData() {
        dataUtil.clear();
    }


}
