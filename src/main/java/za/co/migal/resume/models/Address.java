package za.co.migal.resume.models;

import lombok.Data;

import java.io.Serializable;

@Data
public class Address  implements Serializable {
    String streetName;
    String suburb;
    String city;
    String country;
}
