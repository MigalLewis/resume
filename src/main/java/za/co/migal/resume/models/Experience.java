package za.co.migal.resume.models;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.time.LocalDate;
import java.util.List;

@Data
public class Experience {
    private String id;
    private String image;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern ="yyyy-MM-dd")
    private LocalDate dateFrom;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern ="yyyy-MM-dd")
    private LocalDate dateTo;
    private String company;
    private String position;
    private String description;
    private List<Project> projects;

    @Override
    public String toString(){
        return image;
    }

    @Data
    public static class Project {
        private String name;
        private String emoticon;
        private String role;
        private String shortDescription;
        private String details;
        private List<String> technologies;
    }
}