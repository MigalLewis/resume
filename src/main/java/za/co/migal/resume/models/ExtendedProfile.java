package za.co.migal.resume.models;

import lombok.Data;
import org.springframework.data.annotation.Id;

import java.io.Serializable;
import java.util.List;

@Data
public class ExtendedProfile extends Profile implements Serializable {
    @Id
    String id;
    private String message;
    private List<GoogleItem> googleItems;
    private Hobby hobbies;
    private List<Skill> skills;
    private List<Experience> experienceList;
    private List<Integer> favoritePokemonNo;

    @Data
    public static class GoogleItem implements Serializable {
        private int id;
        private String name;
        private String image;
        private String extraInfo;
        private int rank;
        private List<GoogleExample> googleExamples;
    }
    @Data
    public static class Hobby implements Serializable {
        private List<String> names;
        private String collage;
    }

    @Data
    public static class GoogleExample implements Serializable {
        private String name;
        private String image;
        private String extraInfo;
    }
}
