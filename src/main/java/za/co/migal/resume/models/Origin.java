package za.co.migal.resume.models;

import lombok.Data;

import java.util.List;

@Data
public class Origin {
    private String apiMapping;
    private List<String> urls;
}
