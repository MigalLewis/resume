package za.co.migal.resume.models;

import lombok.Data;

import java.util.List;

@Data
public class Pokemon {
    private int id;
    private String name;
    private String image;
    private String extraInfo;
    private String weight;
    private String height;
    private String species;
    private String description;
    private List<String> types;
}
