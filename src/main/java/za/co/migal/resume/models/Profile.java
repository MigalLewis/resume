package za.co.migal.resume.models;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.time.LocalDate;

@Data
public class Profile {
    @ApiModelProperty(required = true)
    String firstName;
    String lastName;
    String occupation;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern ="yyyy-MM-dd")
    LocalDate dateOfBirth;
    String emailAddress;
    String phoneNumber;
    String linkeden;
    Address address;
    String image;
    String aboutMe;
}
