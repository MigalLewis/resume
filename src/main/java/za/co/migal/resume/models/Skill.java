package za.co.migal.resume.models;

import lombok.Data;

@Data
public class Skill {
    private int id;
    private String name;
    private int score;
    private int yearsExperience;
    private String image;

    @Override
    public String toString() {
        return name;
    }
}
