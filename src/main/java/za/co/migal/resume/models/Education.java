package za.co.migal.resume.models;

import lombok.Data;

@Data
public class Education {
    private String id;
    private String institute;
    private String certificationName;
    private String image;
    private String downloadLink;
}
