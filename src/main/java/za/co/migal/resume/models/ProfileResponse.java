package za.co.migal.resume.models;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class ProfileResponse extends Profile implements Serializable {
    String id;
    List<String> skills;
    List<String> experience;
}
