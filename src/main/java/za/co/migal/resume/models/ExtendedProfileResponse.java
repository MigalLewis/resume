package za.co.migal.resume.models;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class ExtendedProfileResponse extends Profile implements Serializable {
    private String message;
    private List<Pokemon> favouritePokemon;
    private List<ExtendedProfile.GoogleItem> googleItems;
    private List<ExtendedProfile.Hobby> hobbies;
    private List<Skill> skills;
    private List<Experience> experienceList;
}
