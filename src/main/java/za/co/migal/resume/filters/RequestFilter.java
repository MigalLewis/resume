package za.co.migal.resume.filters;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import za.co.migal.resume.models.Origin;
import za.co.migal.resume.util.DataUtil;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.*;

@Component
@Log4j2
public class RequestFilter implements Filter {


    @Value("${api.cors.origins.allowed.file}")
    private String allowedOrigins;
    private DataUtil dataUtil;
    private ObjectMapper objectMapper;

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        log.debug("allowed origin file : "+ allowedOrigins);
        List<Origin> origins = new ArrayList<>();
        try {
            JsonNode jsonNode = dataUtil.getJsonNode(allowedOrigins);
            ArrayNode arrayNode = (ArrayNode)jsonNode.get("allowedOrigins");
            arrayNode.forEach(t -> {
                try {
                    origins.add(objectMapper.treeToValue(t, Origin.class));
                } catch (JsonProcessingException e) {
                    log.error(e);
                }
            });

        } catch (IOException e) {
            log.error(e);
        }
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        HttpServletResponse response = (HttpServletResponse) servletResponse;
        String requestOrigin = request.getHeader("Origin");
        if(requestOrigin!=null && !requestOrigin.isEmpty()) {
            log.info("Originating from "+requestOrigin);
            Optional<Origin> origin = origins.stream().filter(o -> request.getRequestURI().contains(o.getApiMapping())).findFirst();

            if(!origin.isPresent()){
                log.info("No mapping for "+request.getRequestURI()+" found");
            }
            else {
                log.info("mapping for "+request.getRequestURI()+" found");

                Optional<String> urlOrigin = origin.get().getUrls().stream().filter(url ->
                        requestOrigin.startsWith(url)).findFirst();
                if(urlOrigin.isPresent() && !urlOrigin.get().isEmpty()) {
                    log.info("allowing url "+ requestOrigin);
                    response.setHeader("Access-Control-Allow-Origin", urlOrigin.get());
                    response.setHeader("Vary", "Origin");
                } else {
                    log.info("not allowing url "+ requestOrigin + "could not be found");
                }
            }
        }

        filterChain.doFilter(request,response);
    }

    @Autowired
    public void setDataUtil(DataUtil dataUtil) {
        this.dataUtil = dataUtil;
    }
    @Autowired
    public void setObjectMapper(ObjectMapper objectMapper) {
        this.objectMapper = objectMapper;
    }
    public void setAllowedOrigins(String allowedOrigins) {
        this.allowedOrigins = allowedOrigins;
    }
}
