package za.co.migal.resume.services;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.modelmapper.ModelMapper;
import za.co.migal.resume.models.Address;
import za.co.migal.resume.models.ExtendedProfile;
import za.co.migal.resume.models.ProfileResponse;
import za.co.migal.resume.repository.ExtendedProfileRepository;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.*;

class AboutMeServiceImplTest {
    private ExtendedProfileRepository extendedProfileRepository;
    private AboutMeServiceImpl aboutMeService;
    private ModelMapper modelMapper;

    @BeforeEach
    void setUp() {
        extendedProfileRepository = Mockito.mock(ExtendedProfileRepository.class);

        modelMapper = new ModelMapper();
        aboutMeService = new AboutMeServiceImpl();
        aboutMeService.setExtendedProfileRepository(extendedProfileRepository);
        aboutMeService.setModelMapper(modelMapper);
    }

    @Test
    void getSummary() {
        mockMigalsExtendedProfile();

        String name = "Migal";

        ProfileResponse expResult = new ProfileResponse();
        expResult.setAboutMe("this is about me");
        expResult.setAddress(new Address());
        expResult.getAddress().setStreetName("6 wavely street");
        expResult.getAddress().setCity("Palet Town");
        expResult.setDateOfBirth(LocalDate.of(1990 ,7, 5));
        expResult.setEmailAddress("migal@gmail.com");
        expResult.setFirstName("Migal");
        expResult.setLastName("Lewis");
        expResult.setImage("");
        expResult.setOccupation("Software Developer/ Tech Lead");
        expResult.setPhoneNumber("074 863 9773");
        expResult.setId("1");

        ProfileResponse result = aboutMeService.getSummary(name);

        assertEquals(expResult, result);
    }

    @Test
    void addExtendedProfile() {
        mockSaveMigalsExtendedProfile();

        ExtendedProfile extendedProfile = getMigalsExtendedProfile();

        String expResult = "1";
        String result = aboutMeService.addExtendedProfile(extendedProfile);

        assertEquals(expResult, result);
    }




    void mockMigalsExtendedProfile() {

        ExtendedProfile extendedProfile = new ExtendedProfile() ;
        extendedProfile.setAboutMe("this is about me");
        extendedProfile.setAddress(new Address());
        extendedProfile.getAddress().setStreetName("6 wavely street");
        extendedProfile.getAddress().setCity("Palet Town");
        extendedProfile.setDateOfBirth(LocalDate.of(1990,7,5));
        extendedProfile.setEmailAddress("migal@gmail.com");
        extendedProfile.setFirstName("Migal");
        extendedProfile.setLastName("Lewis");
        extendedProfile.setImage("");
        extendedProfile.setOccupation("Software Developer/ Tech Lead");
        extendedProfile.setPhoneNumber("074 863 9773");
        extendedProfile.setId("1");

        String firstName ="Migal";

        Mockito.when(extendedProfileRepository.findByFirstName(firstName))
                .thenReturn(extendedProfile);
    }
    void mockSaveMigalsExtendedProfile() {
        ExtendedProfile extendedProfile = getMigalsExtendedProfile();


        Mockito.when(extendedProfileRepository.save(extendedProfile))
                .thenReturn(extendedProfile);
    }


    ExtendedProfile getMigalsExtendedProfile() {
        ExtendedProfile profile = new ExtendedProfile();
        profile.setAboutMe("this is about me");
        profile.setAddress(new Address());
        profile.getAddress().setStreetName("6 wavely street");
        profile.getAddress().setCity("Palet Town");
        profile.setDateOfBirth(LocalDate.of(1990,7,5));
        profile.setEmailAddress("migal@gmail.com");
        profile.setFirstName("Migal");
        profile.setLastName("Lewis");
        profile.setImage("");
        profile.setOccupation("Software Developer/ Tech Lead");
        profile.setPhoneNumber("074 863 9773");
        profile.setId("1");

        profile.setFavoritePokemonNo(Arrays.asList(4,197,330,94,448,778));

        profile.setGoogleItems(new ArrayList<>());
        profile.getGoogleItems().add(new ExtendedProfile.GoogleItem());
        profile.getGoogleItems().get(0).setId(1);
        profile.getGoogleItems().get(0).setName("Technology");
        profile.getGoogleItems().get(0).setExtraInfo("always interested in the latest tech");
        profile.getGoogleItems().get(0)
                .setImage("https://images.pexels.com/photos/834949/pexels-photo-834949.jpeg?" +
                        "auto=compress&cs=tinysrgb&dpr=2&h=50&w=50");
        profile.getGoogleItems().add(new ExtendedProfile.GoogleItem());
        profile.getGoogleItems().get(1).setRank(1);
        profile.getGoogleItems().get(1).setId(2);
        profile.getGoogleItems().get(1).setName("Software");
        profile.getGoogleItems().get(1).setExtraInfo("stackoverflow, codepen , " +
                "medium thank you for the help you have given me through my career");
        profile.getGoogleItems().get(1)
                .setImage("https://images.pexels.com/photos/546819/pexels-photo-546819.jpeg?" +
                        "auto=compress&cs=tinysrgb&dpr=2&h=50&w=50");
        profile.getGoogleItems().add(new ExtendedProfile.GoogleItem());
        profile.getGoogleItems().get(1).setRank(2);
        profile.getGoogleItems().get(2).setId(3);
        profile.getGoogleItems().get(2).setRank(3);
        profile.getGoogleItems().get(2).setName("Icons");
        profile.getGoogleItems().get(2).setExtraInfo("every now and then it is nice to just read on some" +
                " inspirational things");
        profile.getGoogleItems().get(2)
                .setImage("https://images.pexels.com/photos/6633/car-superhero-symbol-batman.jpg?" +
                        "auto=compress&cs=tinysrgb&dpr=2&h=50&w=50");
        profile.getGoogleItems().add(new ExtendedProfile.GoogleItem());
        profile.getGoogleItems().get(3).setId(4);
        profile.getGoogleItems().get(3).setRank(4);
        profile.getGoogleItems().get(3).setName("Youtube");
        profile.getGoogleItems().get(3).setExtraInfo("really great place for learning, and some entertainment");
        profile.getGoogleItems().get(3)
                .setImage("https://i.pinimg.com/originals/2d/2b/e2/2d2be2421911037d80f9921dc29d54c2.jpg");
        profile.getGoogleItems().add(new ExtendedProfile.GoogleItem());
        profile.getGoogleItems().get(4).setId(5);
        profile.getGoogleItems().get(4).setRank(5);
        profile.getGoogleItems().get(4).setName("DIY");
        profile.getGoogleItems().get(4).setExtraInfo("love building and trying ouy new things");
        profile.getGoogleItems().get(4)
                .setImage("https://images.pexels.com/photos/301703/pexels-photo-301703.jpeg?" +
                        "auto=compress&cs=tinysrgb&dpr=2&h=50&w=50");
        profile.getGoogleItems().add(new ExtendedProfile.GoogleItem());
        profile.getGoogleItems().get(5).setId(6);
        profile.getGoogleItems().get(5).setRank(6);
        profile.getGoogleItems().get(5).setName("Netflix");
        profile.getGoogleItems().get(5).setExtraInfo("I watch atleast one series a day," +
                " Stranger things has to be my favourite from netflix");
        profile.getGoogleItems().get(5)
                .setImage("https://lh3.googleusercontent.com/" +
                        "TBRwjS_qfJCSj1m7zZB93FnpJM5fSpMA_wUlFDLxWAb45T9RmwBvQd5cWR5viJJOhkI");
        profile.getGoogleItems().add(new ExtendedProfile.GoogleItem());
        profile.getGoogleItems().get(6).setId(8);
        profile.getGoogleItems().get(6).setRank(8);
        profile.getGoogleItems().get(6).setName("Soccer/Football");
        profile.getGoogleItems().get(6).setExtraInfo("Latest transfer gossip, Man united , premier league and uefa");
        profile.getGoogleItems().get(6)
                .setImage("https://images.pexels.com/photos/47730/the-ball-stadion-football-the-pitch-47730.jpeg?" +
                        "auto=compress&cs=tinysrgb&dpr=2&h=50&w=50");
        profile.getGoogleItems().add(new ExtendedProfile.GoogleItem());
        profile.getGoogleItems().get(7).setId(7);
        profile.getGoogleItems().get(7).setRank(7);
        profile.getGoogleItems().get(7).setName("Animals");
        profile.getGoogleItems().get(7).setExtraInfo("Whales, Leopard, Octopus, Dogs and Wolves are some of my favourite");
        profile.getGoogleItems().get(7)
                .setImage("https://images.pexels.com/photos/46254/leopard-wildcat-big-cat-botswana-46254.jpeg?" +
                        "auto=compress&cs=tinysrgb&dpr=2&h=50&w=50");


        profile.setHobbies(new ExtendedProfile.Hobby());
        profile.getHobbies().setCollage("../../assets/images/HobbyCollage.png");
        profile.getHobbies().setNames(Arrays.asList("Draw, Photography","Cooking","Traveling"));

        profile.setMessage("My definition of a successful person is a person " +
                "who has changed the world in someway");
        return  profile;
    }


}