package za.co.migal.resume.services;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.modelmapper.ModelMapper;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.web.client.RestTemplate;
import za.co.migal.resume.models.Pokemon;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;

class PokemonServiceImplTest {
    private RestTemplate restTemplate;
    private PokemonServiceImpl pokemonService;
    private ObjectMapper objectMapper;

    @BeforeEach
    void setUp() {
        this.objectMapper = new ObjectMapper();
        this.restTemplate = new RestTemplate();
        this.pokemonService = new PokemonServiceImpl();

        this.pokemonService.setRestTemplate(restTemplate);

        ReflectionTestUtils.setField(pokemonService,"baseUrl", "https://pokeapi.co/api/v2" );

    }

    @Test
    void getPokemonData() throws URISyntaxException {
        this.pokemonService.getPokemonData(94);
    }


    @Test
    void mapBasicPokemonDetails() {
    }

    @Test
    void mapBasicPokemonDetailsWithTwoTypes() throws IOException {
        Pokemon result = new Pokemon();
        pokemonService.mapBasicPokemonDetails(result, getJsonNode("PokemonApiGengar.json"));

        Pokemon expResult = new Pokemon();
        expResult.setId(94);
        expResult.setName("gengar");
        expResult.setImage("https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/94.png");
        expResult.setWeight("405");
        expResult.setHeight("15");
        expResult.setTypes(new ArrayList<>());
        expResult.getTypes().add("poison");
        expResult.getTypes().add("ghost");
        assertEquals(expResult, result);
    }

    @Test
    void mapPokemonSpeciesDetails() {
    }

    JsonNode getJsonNode(String fileName) throws IOException {
        return objectMapper.readTree(getFileAsString(fileName));
    }

    String getFileAsString(String fileName) throws IOException {
        ClassLoader classLoader = getClass().getClassLoader();
        File file = new File(classLoader.getResource("data/"+fileName).getFile());
        return new String(Files.readAllBytes(file.toPath()));
    }
}