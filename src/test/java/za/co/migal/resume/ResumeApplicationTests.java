package za.co.migal.resume;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import org.junit.jupiter.api.*;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.*;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.web.client.MockRestServiceServer;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.context.WebApplicationContext;
import za.co.migal.resume.filters.RequestFilter;
import za.co.migal.resume.models.*;
import za.co.migal.resume.repository.EducationRepository;
import za.co.migal.resume.repository.ExtendedProfileRepository;
import za.co.migal.resume.repository.ProfileRepository;
import za.co.migal.resume.util.DataUtil;

import javax.servlet.Filter;
import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
class ResumeApplicationTests {

    private ProfileRepository profileRepository;
    private ExtendedProfileRepository extendedProfileRepository;
    private EducationRepository educationRepository;
    @Autowired
    private WebApplicationContext context;
    @Autowired
    private MockMvc mockMvc;
    private ObjectMapper objectMapper;
    private DataUtil dataUtil;
    private RequestFilter requestFilter;

    @Value("${api.cors.origins.allowed.file}")
    String origins;

    @MockBean
    private RestTemplate restTemplate;
    private MockRestServiceServer mockServer;

    @BeforeEach
    void setUp() {
        objectMapper = (ObjectMapper) context.getBean(("objectMapper"));;
        profileRepository = (ProfileRepository) context.getBean("profileRepository");
        extendedProfileRepository = (ExtendedProfileRepository) context.getBean(("extendedProfileRepository"));
        educationRepository = (EducationRepository) context.getBean(("educationRepository"));
        dataUtil = (DataUtil) context.getBean(("dataUtil"));
        mockServer = MockRestServiceServer.createServer(restTemplate);
        requestFilter = (RequestFilter) context.getBean("requestFilter");

    }



    @AfterEach
    void clean() {
        extendedProfileRepository.deleteAll();
        educationRepository.deleteAll();
    }

    @Test
    void contextLoads() {
        assertTrue(context.containsBean("objectMapper"));
        assertTrue(context.containsBean("requestFilter"));
        assertTrue(context.containsBean("extendedProfileRepository"));
        assertTrue(context.containsBean("educationRepository"));
        assertTrue(context.containsBean("profileRepository"));
        assertTrue(context.containsBean("aboutMeService"));
        assertTrue(context.containsBean("pokemonService"));
        assertTrue(context.containsBean("dataUtil"));
        assertTrue(context.containsBean("modelMapper"));
    }

    @Test
    void testGetProfile() throws Exception {
        initializeProfile();

        String expResult = getFileAsString("BasicProfileResponse.json");
        String name = "Migal";


        MvcResult mvcResult = this.mockMvc.perform(get("/about/me/profile/basic/" + name)
                .header("origin","http://localhost:4200")).
                andExpect(status().isOk()).andReturn();
        String result = mvcResult.getResponse().getContentAsString();
        assertEquals(expResult, result);
    }

    @DisplayName("Given a Request as a ProfileResponse Object mapped to json String " +
            "When Post /about/me/profile/basic called with request" +
            "Then result status should be 404")
    @Test
    void testPostProfileObject() throws Exception {
        ProfileResponse profileResponse = getMigalsProfile();
        String requestJson = objectMapper.writeValueAsString(profileResponse);


        String expResult = "{\"id\":\"1\",\"firstName\":\"Migal\",\"lastName\":\"Lewis\"," +
                "\"occupation\":\"Software Developer/ Tech Lead\",\"dateOfBirth\":\"1990-07-05\"," +
                "\"emailAddress\":\"migal@gmail.com\",\"phoneNumber\":\"074 863 9773\"," +
                "\"address\":{\"streetName\":\"6 wavely street\",\"suburb\":null," +
                "\"city\":\"Palet Town\",\"country\":null}," +
                "\"image\":\"\",\"aboutMe\":\"this is about me\"}";

        this.mockMvc.perform(post("/about/me/profile/basic").
                header("origin","http://localhost:4200").
                contentType("application/json").
                content(requestJson)).
                andExpect(status().isNotFound()).andReturn();
    }

    @DisplayName("Given a Request comes as a json String " +
            "When /about/me/profile called with request" +
            "Then result status should be 404")
    @Test
    void testPostProfile_NoPorofile() throws Exception {
        String requestJson = "{" +
                "\"id\":\"1\"," +
                "\"firstName\":\"Migal\"," +
                "\"lastName\":\"Lewis\"," +
                "\"occupation\":\"Software Developer/ Tech Lead\"," +
                "\"dateOfBirth\":\"1990 07 05\"," +
                "\"emailAddress\":\"migal@gmail.com\"," +
                "\"phoneNumber\":\"074 863 9773\"," +
                "\"address\":{" +
                "\"streetName\":\"6 wavely street\"," +
                "\"suburb\":null," +
                "\"city\":\"Palet Town\"," +
                "\"country\":null" +
                "}," +
                "\"image\":\"\"," +
                "\"aboutMe\":\"this is about me\"" +
                "}";

        MvcResult mvcResult =
                this.mockMvc.perform(post("/about/me/profile").
                        header("origin","http://localhost:4200").
                        contentType("application/json").
                        content(requestJson)).
                        andExpect(status().isNotFound()).andReturn();
        MockHttpServletResponse result = mvcResult.getResponse();
    }

    @DisplayName("Given a Request comes as a json String " +
            "and Date Format is incorrect" +
            "When /about/me/profile/extended called with request" +
            "Then result status should be 400")
    @Test
    void testPostProfile_incorrectDate() throws Exception {
        String requestJson = "{" +
                "\"id\":\"1\"," +
                "\"firstName\":\"Migal\"," +
                "\"lastName\":\"Lewis\"," +
                "\"occupation\":\"Software Developer/ Tech Lead\"," +
                "\"dateOfBirth\":\"1990 07 05\"," +
                "\"emailAddress\":\"migal@gmail.com\"," +
                "\"phoneNumber\":\"074 863 9773\"," +
                "\"address\":{" +
                "\"streetName\":\"6 wavely street\"," +
                "\"suburb\":null," +
                "\"city\":\"Palet Town\"," +
                "\"country\":null" +
                "}," +
                "\"image\":\"\"," +
                "\"aboutMe\":\"this is about me\"" +
                "}";

        MvcResult mvcResult =
                this.mockMvc.perform(post("/about/me/profile/extended").
                        header("origin","http://localhost:4200").
                        contentType("application/json").
                        content(requestJson)).
                        andExpect(status().isBadRequest()).andReturn();
        MockHttpServletResponse result = mvcResult.getResponse();
    }

    @DisplayName("Given a Request as a ProfileResponse Object mapped to json String " +
            "When /about/me/profile/extended called with request" +
            "Then result status should be 200")
    @Test
    void testPostExtendedProfileObject() throws Exception {
        String requestJson = getFileAsString("MigalExtendedProfileRequest.json");

        String expResult = "1";

        MvcResult mvcResult =
                this.mockMvc.perform(post("/about/me/profile/extended").
                        header("origin","http://localhost:4200").
                        contentType("application/json").
                        content(requestJson)).
                        andExpect(status().isOk()).andReturn();

        MockHttpServletResponse result = mvcResult.getResponse();
        assertEquals(expResult, result.getContentAsString());
        assertEquals(200, result.getStatus());
    }

    @DisplayName("Given a Request comes as a json String " +
            "When /about/me/profile/extended called with request" +
            "Then result status should be 200")
    @Test
    void testPostExtendedProfile() throws Exception {
        String requestJson = getFileAsString("MigalExtendedProfileRequest.json");

        String expResult = "1";

        MvcResult mvcResult =
                this.mockMvc.perform(post("/about/me/profile/extended").
                        header("origin","http://localhost:4200").
                        contentType("application/json").
                        content(requestJson)).
                        andExpect(status().isOk()).andReturn();
        MockHttpServletResponse result = mvcResult.getResponse();
        assertEquals("text/plain;charset=ISO-8859-1", result.getContentType());
        assertEquals(expResult, result.getContentAsString());
    }

    @DisplayName("Given a Request as a ProfileResponse Object mapped to json String " +
            "When /about/me/profile/extended called with request" +
            "Then data should be added to the database")
    @Test
    void testPostExtendedProfileDataSaved() throws Exception {
        ExtendedProfile profile = getMigalsExtendedProfile();
        String requestJson = objectMapper.writeValueAsString(profile);

        MvcResult mvcResult =
                this.mockMvc.perform(post("/about/me/profile/extended").
                        contentType("application/json").
                        header("origin","http://localhost:4200").
                        content(requestJson)).
                        andExpect(status().isOk()).andReturn();
        MockHttpServletResponse result = mvcResult.getResponse();
        ExtendedProfile expResult = extendedProfileRepository.findById("1").get();

        assertEquals("text/plain;charset=ISO-8859-1", result.getContentType());
        assertEquals(200, result.getStatus());
        assertEquals(profile, expResult);
    }

    @DisplayName("Given and extended profile in the database" +
            "When /about/me/profile/extended called with request" +
            "Then data should be returned")
    @Test
    void testGetExtendedProfile() throws Exception {
        initializeProfile();
        String name = "Migal";




        MvcResult mvcResult =
                this.mockMvc.perform(get("/about/me/profile/extended/" + name).
                        header("origin","http://localhost:4200")).
                        andExpect(status().isOk()).andReturn();
        MockHttpServletResponse servletResponse = mvcResult.getResponse();
        String result = servletResponse.getContentAsString();

        String expResult = getFileAsString("MigalExtendedProfileResponse.json");

        assertEquals(expResult, result);
        assertEquals("application/json", servletResponse.getContentType());
    }

    @DisplayName("Given a  " +
            "When /data/initialize called with request" +
            "Then data should be added to the database")
    @Test
    void testPostInitializeData() throws Exception {

        this.mockMvc.perform(post("/data/initialize")
                .header("origin","http://localhost:4200")).

                andExpect(status().isOk()).andReturn();

        List<ExtendedProfile> extendedProfileResult = extendedProfileRepository.findAll();
        int expExtendedProfileResult = 1;


        List<Education> educationResult = educationRepository.findAll();
        int expEducationResult = 3;

        assertEquals(expExtendedProfileResult, extendedProfileResult.size());
        assertEquals(expEducationResult, educationResult.size());
    }

    @DisplayName("Given a  " +
            "When /about/me/experience called with request" +
            "Then result should be ")
    @Test
    void getExperience() throws Exception {
        initializeProfile();

        MvcResult mvcResult = this.mockMvc.perform(get("/about/me/experience")
                .header("origin","http://localhost:4200")).
                andExpect(status().isOk()).andReturn();

        String experienceResult =  mvcResult.getResponse().getContentAsString() ;
        String expExperienceResult = objectMapper.writeValueAsString(extendedProfileRepository.findAll().get(0).getExperienceList());

        assertEquals(expExperienceResult, experienceResult);
    }

    @Test
    void getEducation() throws Exception {
        initializeProfile();

        MvcResult mvcResult = this.mockMvc.perform(get("/about/me/education")
                .header("origin","http://localhost:4200")).
                andExpect(status().isOk()).andReturn();

        String educationResult =  mvcResult.getResponse().getContentAsString() ;
        String expEducationResult = objectMapper.writeValueAsString(educationRepository.findAll());

        assertEquals(educationResult, expEducationResult);
    }

    @Test
    void getSkills() throws Exception {
        initializeProfile();

        MvcResult mvcResult = this.mockMvc.perform(get("/about/me/skills").
                header("origin","http://localhost:4200")).
                andExpect(status().isOk()).andReturn();

        String educationResult =  mvcResult.getResponse().getContentAsString() ;
        String expEducationResult = objectMapper.writeValueAsString(extendedProfileRepository.findAll().get(0).getSkills());

        assertEquals(educationResult, expEducationResult);
    }


    ProfileResponse getMigalsProfile() {
        ProfileResponse profileResponse = new ProfileResponse();
        profileResponse.setAboutMe("this is about me");
        profileResponse.setAddress(new Address());
        profileResponse.getAddress().setStreetName("6 wavely street");
        profileResponse.getAddress().setCity("Palet Town");
        profileResponse.setDateOfBirth(LocalDate.of(1990, 7, 5));
        profileResponse.setEmailAddress("migal@gmail.com");
        profileResponse.setFirstName("Migal");
        profileResponse.setLastName("Lewis");
        profileResponse.setImage("");
        profileResponse.setOccupation("Software Developer/ Tech Lead");
        profileResponse.setPhoneNumber("074 863 9773");
        profileResponse.setId("1");
        return profileResponse;
    }

    ExtendedProfile getMigalsExtendedProfile() {
        ExtendedProfile profile = new ExtendedProfile();
        profile.setAboutMe("this is about me");
        profile.setAddress(new Address());
        profile.getAddress().setStreetName("6 wavely street");
        profile.getAddress().setCity("Palet Town");
        profile.setDateOfBirth(LocalDate.of(1990, 7, 5));
        profile.setEmailAddress("migal@gmail.com");
        profile.setFirstName("Migal");
        profile.setLastName("Lewis");
        profile.setImage("");
        profile.setOccupation("Software Developer/ Tech Lead");
        profile.setPhoneNumber("074 863 9773");
        profile.setId("1");

        profile.setFavoritePokemonNo(Arrays.asList(4,197,330,94,448,778));

        profile.setGoogleItems(new ArrayList<>());
        profile.getGoogleItems().add(new ExtendedProfile.GoogleItem());
        profile.getGoogleItems().get(0).setId(1);
        profile.getGoogleItems().get(0).setName("Technology");
        profile.getGoogleItems().get(0).setExtraInfo("always interested in the latest tech");
        profile.getGoogleItems().get(0)
                .setImage("https://images.pexels.com/photos/834949/pexels-photo-834949.jpeg?" +
                        "auto=compress&cs=tinysrgb&dpr=2&h=50&w=50");
        profile.getGoogleItems().get(0).setGoogleExamples(new ArrayList<>());
        profile.getGoogleItems().get(0).getGoogleExamples().add(new ExtendedProfile.GoogleExample());
        profile.getGoogleItems().get(0).getGoogleExamples().get(0).setExtraInfo("");
        profile.getGoogleItems().get(0).getGoogleExamples().get(0).setImage("image");
        profile.getGoogleItems().get(0).getGoogleExamples().get(0).setName("Virtual Reality");
        profile.getGoogleItems().get(0).getGoogleExamples().add(new ExtendedProfile.GoogleExample());
        profile.getGoogleItems().get(0).getGoogleExamples().get(1).setExtraInfo("");
        profile.getGoogleItems().get(0).getGoogleExamples().get(1).setImage("image");
        profile.getGoogleItems().get(0).getGoogleExamples().get(1).setName("3D Printing");
        profile.getGoogleItems().add(new ExtendedProfile.GoogleItem());
        profile.getGoogleItems().get(1).setRank(1);
        profile.getGoogleItems().get(1).setId(2);
        profile.getGoogleItems().get(1).setName("Software");
        profile.getGoogleItems().get(1).setExtraInfo("stackoverflow, codepen , " +
                "medium thank you for the help you have given me through my career");
        profile.getGoogleItems().get(1)
                .setImage("https://images.pexels.com/photos/546819/pexels-photo-546819.jpeg?" +
                        "auto=compress&cs=tinysrgb&dpr=2&h=50&w=50");
        profile.getGoogleItems().get(1).setGoogleExamples(new ArrayList<>());
        profile.getGoogleItems().get(1).getGoogleExamples().add(new ExtendedProfile.GoogleExample());
        profile.getGoogleItems().get(1).getGoogleExamples().get(0).setExtraInfo("");
        profile.getGoogleItems().get(1).getGoogleExamples().get(0).setImage("image");
        profile.getGoogleItems().get(1).getGoogleExamples().get(0).setName("Java");
        profile.getGoogleItems().get(1).getGoogleExamples().add(new ExtendedProfile.GoogleExample());
        profile.getGoogleItems().get(1).getGoogleExamples().get(1).setExtraInfo("");
        profile.getGoogleItems().get(1).getGoogleExamples().get(1).setImage("image");
        profile.getGoogleItems().get(1).getGoogleExamples().get(1).setName("Angular");


        profile.setMessage("My definition of a successful person is a person " +
                "who has changed the world in someway");
        return profile;
    }


    void initializeProfile() throws IOException {

        dataUtil.initializeExtendedProfile();

        Arrays.asList(4,197,330,94,448,778).stream().forEach(no -> {
            try {
                mockPokemon(no);
            } catch (URISyntaxException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
    }



    private void mockPokemon(int no) throws URISyntaxException, IOException {

        Mockito.when(restTemplate.exchange(Mockito.eq(new URI("https://pokeapi.co/api/v2/pokemon/"+no)),
                Mockito.eq(HttpMethod.GET),
                Mockito.any(HttpEntity.class),
                Mockito.eq(JsonNode.class)))
                .thenReturn(new ResponseEntity(getJsonNode("pokemonApiResponse" + no + ".json"),
                        HttpStatus.OK));

        Mockito.when(restTemplate.exchange(Mockito.eq(new URI("https://pokeapi.co/api/v2/pokemon-species/"+no)),
                Mockito.eq(HttpMethod.GET),
                Mockito.any(HttpEntity.class),
                Mockito.eq(JsonNode.class)))
                .thenReturn(new ResponseEntity(getJsonNode("pokemonSpeciesApiResponse" + no + ".json"),
                        HttpStatus.OK));
    }
    private List<Pokemon> pokemonExpectedResult() {
        List<Pokemon> pokemonList = new ArrayList<>();

        pokemonList.add(new Pokemon());
        pokemonList.get(0).setExtraInfo("Proffesor Oak, Charmander will do");
        pokemonList.get(0).setId(4);
        pokemonList.get(0).setName("Charmander");
        pokemonList.get(0).
                setImage("https://assets.pokemon.com/assets/cms2/img/pokedex/full/004.png");
        pokemonList.add(new Pokemon());
        pokemonList.get(1).setExtraInfo("This is what I am evolving Eevee to");
        pokemonList.get(1).setId(197);
        pokemonList.get(1).setName("Umbreon");
        pokemonList.get(1).
                setImage("https://assets.pokemon.com/assets/cms2/img/pokedex/full/197.png");
        pokemonList.add(new Pokemon());
        pokemonList.get(2).setId(330);
        pokemonList.get(2).setName("Flygon");
        pokemonList.get(2).
                setImage("https://assets.pokemon.com/assets/cms2/img/pokedex/full/330.png");
        pokemonList.add(new Pokemon());
        pokemonList.get(3).setExtraInfo("We all know Gengar is great for battles");
        pokemonList.get(3).setId(94);
        pokemonList.get(3).setName("Gengar");
        pokemonList.get(3).
                setImage("https://assets.pokemon.com/assets/cms2/img/pokedex/full/094.png");
        pokemonList.add(new Pokemon());
        pokemonList.get(4).setId(448);
        pokemonList.get(4).setName("Lucario");
        pokemonList.get(4).
                setImage("https://assets.pokemon.com/assets/cms2/img/pokedex/full/448.png");
        pokemonList.add(new Pokemon());
        pokemonList.get(5).setExtraInfo("What a traumatic story, I will look after you buddy");
        pokemonList.get(5).setId(778);
        pokemonList.get(5).setName("Mimikyu");
        pokemonList.get(5).
                setImage("https://assets.pokemon.com/assets/cms2/img/pokedex/full/778.png");
        return pokemonList;

    }


    JsonNode getJsonNode(String fileName) throws IOException {
        return objectMapper.readTree(getFileAsString(fileName));
    }

    String getFileAsString(String fileName) throws IOException {
        ClassLoader classLoader = getClass().getClassLoader();
        File file = new File(classLoader.getResource("data/"+fileName).getFile());
        return new String(Files.readAllBytes(file.toPath()));
    }


}
